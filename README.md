# deu-cookie-consent

A Vue component that represents DEU's cookie banner and initializes analytics scripts.

### Requirements

This package requires the following package to be installed in order to be working correctly:

- `npm install @piveau/piveau-universal-piwik@2`

### Installation

```bash
npm install @piveau/piveau-universal-piwik@2 @deu/deu-cookie-consent
```

### Usage Example

```html
<!-- css import for when you want to import the component css into your css file/files  -->
@import '/path/to/node_modules/@deu/deu-cookie-consent.css';

<!-- css import for when you're importing the css directly in your js  -->
import '@deu/deu-cookie-consent/dist/deu-cookie-consent.css'

import DeuCookieConsent from '@deu/deu-cookie-consent'
import UniversalPiwik from '@piveau/piveau-universal-piwik'

Vue.use(UniversalPiwik, {/*Universal Piwik config here*/})
Vue.component('deu-cookie-consent', DeuCookieConsent)
```

```html
<deu-cookie-consent :piwik-instance="$piwik" />
```

## Developing

You can see changes to the source code locally by running the demo. For testing this plugin as an npm package on another project, build and install this plugin like this:
```
npm run build:lib
cd ../{OTHER_PROJECT_FOLDER}
npm install ../{THIS_PROJECT_ROOT}
```

Please refer to [the contributing guide](./CONTRIBUTING.md) before submitting changes to the code.


## Updating the Snapshot

```
npm run test:unit -- -u
```

## Project setup
```
npm install
```

### Run unit tests
```
npm run test:unit
```

### Compiles and minifies for production
```
npm run build:lib
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
