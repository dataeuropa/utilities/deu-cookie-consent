import Vue from 'vue'
import App from './App.vue'

import UniversalPiwik from '@piveau/piveau-universal-piwik';

Vue.use(UniversalPiwik, {
  router: null,
  isPiwikPro: process.env.VUE_APP_TRACKER_IS_PIWIK_PRO && process.env.VUE_APP_TRACKER_IS_PIWIK_PRO !== 'false',
  trackerUrl: process.env.VUE_APP_TRACKER_TRACKER_URL || 'http://localhost:9090',
  siteId: process.env.VUE_APP_TRACKER_SITE_ID || '1',
  pageViewOptions: { onlyTrackWithLocale: false, delay: 250 },
  debug: process.env.NODE_ENV === 'development',
  verbose: process.env.NODE_ENV === 'development',
  immediate: false,
});

Vue.config.productionTip = false

Vue.prototype.$t = (a) => { // Mocking i18n
  const dotIndex = a.lastIndexOf('.');
  if (dotIndex < 0) return a.toUpperCase();
  return a.substring(dotIndex + 1).toUpperCase();
};

new Vue({
  render: h => h(App),
}).$mount('#app')
