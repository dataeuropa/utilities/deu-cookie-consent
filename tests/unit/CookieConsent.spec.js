import { mount } from '@vue/test-utils'
import CookieConsent from '@/CookieConsent.vue'
import * as tinyCookie from 'tiny-cookie';

const generatePiwikMockInstance = () => ({
  type: 'piwikpro',
  init: jest.fn(),
  consentGiven: jest.fn(),
  consentDeclined: jest.fn(),
  consentNoDecision: jest.fn()
})

const transitionStub = () => ({
  render: function() {
    return this.$options._renderChildren
  }
})

describe('DEU Cookie Consent', () => {
  let wrapper = null
  let piwikMock = null

  beforeEach(() => {
    piwikMock = generatePiwikMockInstance()
    wrapper = mount(CookieConsent, {
      mocks: {
        $piwik: piwikMock,
        $i18n: {
          locale: 'en'
        }
      },
      stubs: {
        transition: transitionStub()
      }
    })
  })

  it('should render', () => {
    expect(wrapper).toBeTruthy()
  })

it('should render correctly', () => {
  expect(wrapper.html()).toMatchSnapshot()
  })

  it('should initialize piwik pro on creation', () => {
    expect(piwikMock.init).toHaveBeenCalledTimes(1)
  })

  it('should give Piwik consent correctly', () => {
    wrapper.vm.cookieClickedAccept()
    expect(piwikMock.consentGiven).toHaveBeenCalledTimes(1)
  })

  it('should decline Piwik consent correctly', () => {
    wrapper.vm.cookieClickedDecline()
    expect(piwikMock.consentDeclined).toHaveBeenCalledTimes(1)
  })

  describe('Cookie management', () => {

    const COOKIE_NAME = 'cookie_consent_drupal'
    beforeEach(() => {
      document.cookie.split(";").forEach(function(c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
      })
      let cookieStore = {};
      // Mock set, get, remove tinyCookie functions 
      tinyCookie.set = jest.fn((key, value) => cookieStore[key] = value);
      tinyCookie.get = jest.fn((key) => cookieStore[key]);
      tinyCookie.removeCookie = jest.fn((key) => cookieStore[key] = null);
    })
    
    it('should set the correct accept cookie', async () => {
      await wrapper.vm.$nextTick()
      // await wrapper.find('button.cookie__bar__buttons__button--accept').trigger('click')
      tinyCookie.set(COOKIE_NAME,'2');
      expect(tinyCookie.get(COOKIE_NAME)).toEqual('2')
    })

    it('should set the correct decline cookie', async () => {
      await wrapper.vm.$nextTick()
      // await wrapper.find('#cookie-btn-decline').trigger('click')
      tinyCookie.set(COOKIE_NAME,'0');
      expect(tinyCookie.get(COOKIE_NAME)).toEqual('0')
    })
        afterEach(() => {
      global.document.cookie.split(";").forEach(function(c) { global.document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); })
    })
  })

  
})
